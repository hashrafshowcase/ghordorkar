<?php

namespace App\Http\Controllers\AreaSearch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\AreaSearch as ArSrch;
use Auth;
use App\Http\Controllers\SaveSearch\SaveSearch;

class AreaSearch extends Controller
{
    public $request;

    const EARTH_RADIUS_IN_KM = 6371;
    const EARTH_RADIUS_IN_MILES = 3959;

    public function __construct
    (
        Request $request
    )
    {
        $this->request = $request;
    }

    public function removeMySearchArea($searchId)
    {
        ArSrch::find($searchId)->delete();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Successfully removed'
        ]);
    }

    public function getCurrentUserSearchArea()
    {
        return response()->json([
            'search_area' => ArSrch::where('user_id', Auth::user()->id)->get()
        ]);
    }

    public static function mapSearch(Request $request, Object $createProperty)
    {
        $radius = 1.0; // radius
        $getDataObject = $request->all();
        $getDataObject = (object) $getDataObject;

        $queryInit = ArSrch::select("*")->with('user')->where('user_id', '!=', $createProperty->user_id);

        $typeArr = ['family', 'mess', 'sublet'];

        if ($createProperty->price > 2000.00) {
            $queryInit = $queryInit->whereBetween('max_price', [2000.00, $createProperty->price]);
        }

        if ($createProperty->size > 100.00) {
            $queryInit = $queryInit->whereBetween('max_size', [100.00, $createProperty->size]);
        }

        if ($createProperty->bedrooms) {
            $queryInit = $queryInit->where('bedrooms', $createProperty->bedrooms);
        }

        if ($createProperty->bathrooms) {
            $queryInit = $queryInit->where('bathrooms', $createProperty->bathrooms);
        }
        

        $users = $queryInit->where('types', $typeArr[$getDataObject->types - 1])->selectRaw(self::getSql(self::EARTH_RADIUS_IN_KM), [
                                                    $request->get('lat'),
                                                    $request->get('lng'),
                                                    $request->get('lat')
                                                ])
                                                ->havingRaw("distance <= ?", [$radius])
                                                ->orderBy('created_at', 'DESC')
                                                ->get("*");
         

        foreach($users as $user) {
            SaveSearch::add(
                $user->user_id, 
                $createProperty->model, 
                $createProperty->id, 
                $createProperty->expiration_date
            );
        }
     
    }

    public static function getSql($r)
    {
        $sql = "( $r * acos( cos( radians(?) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?) )";
        $sql.= " + sin( radians(?) ) * sin( radians( lat ) ) ) ) AS distance";
        return $sql;
    }

    public function createNewSearchArea(Request $request)
    {
        if( ! $this->request->get('address') && 
            ! $this->request->get('lat') &&
            ! $this->request->get('lng')
        ) 
        {
            return reponse()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Address Not Selected Properly'
            ], 404);
        }

        $dataArray = [
            'user_id' =>  Auth::user()->id,
            'types' => $this->setIfNull('types'),
            'max_price' => $this->setIfNull('max_price'),
            'min_price' => 2000,
            'lat' => $this->setIfNull('lat'),
            'lng' => $this->setIfNull('lng'),
            'address' => $this->setIfNull('address'),
            'area' => $this->setIfNull('area'),
            'city' => $this->setIfNull('city'),
            'zip_code' => $this->setIfNull('zip_code'),
            'min_size' => 100,
            'max_size' => $this->setIfNull('max_size'),
            'rooms' => $this->setIfNull('rooms'),
            'bathrooms' => $this->setIfNull('bathrooms'),
            'bedrooms' => $this->setIfNull('bedrooms')
        ];

        ArSrch::create($dataArray);

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Successfully Added'
        ]);

    }

    public function setIfNull($key)
    {
        if ($this->request->get($key)) {
            return $this->request->get($key);
        }

        return null;
    }

    public static function permission()
    {
        if (! Auth::check()) {
            return false;
        }

        $getUserDetails = User::find(Auth::user()->id);
        $getAreaSearchByUser = ArSrch::where('user_id', Auth::user()->id)->get();

        if ($getUserDetails->account_type == 0) {
            return false;
        }

        $getMaxListByAccountType = self::maxSearchList($getUserDetails->account_type);

        if ( count($getAreaSearchByUser) < $getMaxListByAccountType ) {
            return true;
        } else if ( count($getAreaSearchByUser) == $getMaxListByAccountType ) {
            return false;
        }
    }

    public static function maxSearchList(int $key)
    {
        
        $list = [
            1,
            5,
            10
        ];

        return $list[$key - 1];
    }
}
