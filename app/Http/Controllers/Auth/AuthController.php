<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Socialite;
use App\User;
use App\UserProvider;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Mail;
use App\Mail\SignUp;
use App\Http\Controllers\Profile\ProfileController;

class AuthController extends Controller
{

    public $redirectTo = "/";

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = UserProvider::with('user')->where('provider_id', $user->id)->first();

        if ($authUser) {
            return $authUser->user;
        }

        $authEmailChecking = User::where('email', $user->email)->first();

        if ($authEmailChecking) {
            UserProvider::create([
                'user_id' => $authEmailChecking->id,
                'provider' => $provider,
                'provider_id' => $user->id
            ]);

            return $authEmailChecking;
        }

        $createUser =  User::create([
            'uuid'     => Uuid::uuid1()->toString(),
            'name'     => $user->name,
            'email'    => $user->email,
            'avatar'   => $user->avatar,
            'mail_verify_token' => Uuid::uuid1()->toString(),
            'payment_id' => ProfileController::setPaymentID()
        ]);

        UserProvider::create([
            'user_id' => $createUser->id,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);

        // Mail::send('email.signup', function ($message) {

        //     $message->from('ghordorkar@gmail.com', 'Learning Laravel');
    
        //     $message->to('rafsanhashemi@gmail.com')->subject('Learning Laravel test email');
    
        // });

        // // check for failures
        // if (Mail::failures()) {
        //     dd('rafsan');
        // }

        return $createUser;
    }
}
