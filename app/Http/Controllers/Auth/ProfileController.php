<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class ProfileController extends Controller
{
    
    public static function currentUserSubscribed()
    {
        if (! Auth::check()) {
            return false;
        }

        if (Auth::user()->account_type) {
            return true;
        }

        return false;
    }

    public static function isUserSubscribed($id)
    {
        if (! Auth::check()) {
            return false;
        }

        if (User::find($id)->account_type > 0) {
            return true;
        }

        return false;
    }

    public static function getPhoneNumber($id)
    {
        $phoneNumber  = User::find($id)->phone_number;

        if ( $phoneNumber  != NULL ) {
            return $phoneNumber;
        }

        return "+8801XXXXXXX";
    }

}
