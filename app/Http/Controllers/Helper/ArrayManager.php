<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\ExtraDefinedController;
use App\Http\Controllers\Helper\StrManager;
use Auth;

class ArrayManager extends Controller
{
    public static function removeEmptyIndex(Array $array)
    {
        $updateArray = [];

        foreach($array as $value) {
            foreach($value as $property) {
                array_push($updateArray, $property);
            }
        }

        return $updateArray;
    }

    public static function setPropertyAvatar(Object $property)
    {
        if ( count($property->gallery) ) {
            return $property->gallery[0]->value;
        }

        return url('static/image/property_avatar.jpg');
    }

    public static function getModelIndex($key)
    {
        $array  = array_keys(((new ExtraDefinedController())->modelArray));
        return array_search($key, $array) + 1;
    }

    public static function userPackage($key)
    {
        if (! Auth::check()) {
            return false;
        }
        $package = ['Free', 'Basic', 'Extended', 'Pro'];

        return $package[$key->account_type];
    }
}
