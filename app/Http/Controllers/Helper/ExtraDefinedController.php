<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\propertyFamily;
use App\propertyMess;
use App\propertySublet;
use App\User;

class ExtraDefinedController extends Controller
{

    public $modelArray;

    public function __construct()
    {
        $this->modelArray = [
            'App/propertyFamily' => (new propertyFamily()),
            'App/propertyMess'   => (new propertyMess()),
            'App/propertySublet' => (new propertySublet())
        ];
    }

    public static function paymentType() 
    {
        return [
            [
                'id' => 0,
                'label' => 'Free',
                'fee'   => 0
            ],
            [
                'id' => 1,
                'label' => 'Subscribed',
                'fee' => 30
            ],
            [
                'id' => 2,
                'label' => 'Pro',
                'fee' => 60
            ],
            [
                'id' => 3,
                'label' => 'Premium',
                'fee' => 100
            ]
        ];
    }


    public function propertyType()
    {
        return [
            [
                'id' => 1,
                'label' => 'Family House',
                'value' => 'family',
                'model' => (new propertyFamily())
            ],
            [
                'id' => 2,
                'label' => 'Bachelor Mess',
                'value' => 'mess',
                'model' => (new propertyMess())
            ],
            [
                'id' => 3,
                'label' => 'Sublet',
                'value' => 'sublet',
                'model' => (new propertySublet())
            ]
        ];
    }

    public function userAccountType()
    {
        return [
            [
                'id' => 0,
                'label' => 'Free'
            ],
            [
                'id' => 1,
                'label' => 'Subscribed'
            ],
            [
                'id' => 2,
                'label' => 'Pro'
            ],
            [
                'id' => 3,
                'label' => 'Enterprise'
            ]
        ];
    }

    public function propertyRentStatus()
    {
        return [
            [
                'id' => 1,
                'label' => 'Home Rent Post'
            ],
            [
                'id' => 2,
                'label' => 'Request For Post'
            ]
        ];
    }

    public static function currentUserAccountType() {
        if (Auth::check()) {
            return User::find(Auth::user()->id)->account_type;
        }
        return 0;
    }


    public function setModel($key)
    {
        return $this->modelArray[$key];
    }
}
