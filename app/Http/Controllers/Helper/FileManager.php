<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileManager extends Controller
{
    public static function removeFile(String $link)
    {
        $replaceFilePath = str_replace(url('/'), '', $link);
        if (file_exists(public_path($replaceFilePath))) {
            unlink(public_path($replaceFilePath));
        }
    }
}
