<?php

namespace App\Http\Controllers\Helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StrManager extends Controller
{
    public static function changeBackSlash(String $str)
    {
        $changeString = explode('/', $str);
        return implode('\\', $changeString);
    }

    public static function changeTenor(String $str)
    {
        $changeString = explode('\\', $str);
        return implode('/', $changeString);
    }

    public static function setAddress(Object $property)
    {
        $address = [];

        if ($property->address != NULL) {
            $address[] = $property->address;
        }

        if ($property->area != NULL) {
            $address[] = $property->area;
        }

        if ($property->city != NULL) {
            $address[] = $property->city;
        }

        if ($property->zip_code != NULL) {
            $address[] = $property->zip_code;
        }

        return implode(",", $address);
    }

    public static function setPhoneNumber(Bool $bool, String $str)
    {
        if ($bool) {
            return $str;
        }

        return "+8801XXXXXX";
    }
}
