<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Location\Location;
use App\Http\Controllers\Payment\Payment;
use App\Http\Controllers\Helper\ArrayManager;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        return view('home.pages.home', [ 
            'package' => ArrayManager::userPackage(Auth::user()),
            'links' => $this->generateURL(), 
            'isPaid' => Payment::isPaid() ,
            'isFirst' => Payment::isFirst()
        ]);
    }


    public function generateURL()
    {
        $location = Location::make();
        $lat = $location->lat;
        $lng = $location->lon;

        return [
            'family' => url("search-result?s_type=1&q=&lat=$lat&lng=$lng&min_price=&max_price=&min_area=&max_area=&beds=&baths="),
            'mess'   => url("search-result?s_type=2&q=&lat=$lat&lng=$lng&min_price=&max_price=&min_area=&max_area=&beds=&baths="),
            'sublet' => url("search-result?s_type=3&q=&lat=$lat&lng=$lng&min_price=&max_price=&min_area=&max_area=&beds=&baths=")
        ];
    }
}
