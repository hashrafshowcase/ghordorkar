<?php

namespace App\Http\Controllers\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Location extends Controller
{

    public static function make()
    {
        $myPublicIP = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));        
        //dd(->ip);
        return geoip($ip = $myPublicIP);
    }
}
