<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Payment as Pay;
use Auth;
use Carbon\Carbon;

class Payment extends Controller
{
    /**
     * Undocumented function
     *
     * @return boolean
     */
    public static function isPaid()
    {
        if (! Auth::check()) {
            return true;
        }

        $getCurrentUser = User::find(Auth::user()->id);
        $getCurrentUserPayment = Pay::where('user_id', $getCurrentUser->id)->where('end_date', '>=', Carbon::now()->toDateString())
                                                                           ->where('account_type', $getCurrentUser->account_type)  
                                                                           ->where('status', 'active')
                                                                           ->orderBy('created_at', 'DESC')
                                                                           ->first();   

        if ( is_null($getCurrentUserPayment) ) {
            return false;
        } else {
            return true;
        }
    }

    public static function isFirst()
    {
        if (! Auth::check()) {
            return true;
        }

        $getCurrentUser = User::find(Auth::user()->id);
        $getCurrentUserPayment = Pay::where('user_id', $getCurrentUser->id)->get();

        if (! count( $getCurrentUserPayment ) ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function history()
    {
        $getCurrentUser = User::find(Auth::user()->id);
        $getCurrentUserPayment = Pay::where('user_id', $getCurrentUser->id)->orderBy('created_at', 'DESC')
                                                                           ->get();
        return $getCurrentUserPayment;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function validityRemain7days()
    {
        if (! Auth::check()) {
            return true;
        }

        $getCurrentUser = User::find(Auth::user()->id);
        $getCurrentUserPayment = Pay::where('user_id', $getCurrentUser->id)->where('end_date', '>=', Carbon::now()->toDateString())
                                                                           ->where('account_type', $getCurrentUser->account_type)  
                                                                           ->where('status', 'active')
                                                                           ->orderBy('created_at', 'DESC')
                                                                           ->first();   

        if ( is_null($getCurrentUserPayment) ) {
            return false;
        } else {
            $end = Carbon::parse($getCurrentUserPayment->end_date);
            $now = Carbon::now();
            $diff = $end->diffInDays($now);
            if ( $diff <= 7 ) {
                return [
                    'status' => true,
                    'message' => $diff . ' remaining'
                ];
            } else {
                return [
                    'status' => false,
                    'message' => $diff . ' remaining'
                ];
            }
        }
    }

}
