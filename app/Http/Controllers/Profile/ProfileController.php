<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class ProfileController extends Controller
{
    public $user;
    public $request;

    public function __construct
    (
        User $user,
        Request $request
    )
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->request = $request;
    }

    public static function setPaymentID()
    {
        $latestUser = User::orderBy('created_at', 'desc')->first();

        if ($latestUser) {
            if ($latestUser->payment_id) {
                return $latestUser->payment_id + 1;
            }
        } else {
            return 10000;
        }
    }

    public function updateProfile(Request $request)
    {
        $dataArray = [
            'name' => $request->get('name'),
            'occupation' => $request->get('occupation'),
            'phone_number' => $request->get('phone_number'),
            'notification_email' => $request->get('notification_email'),
            'notification_mobile' => $request->get('notification_mobile'),
            'about_me' => $request->get('about_me')

        ];

        $this->user->where('id', Auth::user()->id)->update($dataArray);

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Successfully Updated!'
        ]);
    }
}
