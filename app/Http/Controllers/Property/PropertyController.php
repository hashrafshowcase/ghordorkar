<?php

namespace App\Http\Controllers\Property;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\propertyFamily;
use App\propertyMess;
use App\propertySublet;
use App\propertyGallery;
use Auth;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\Helper\ExtraDefinedController;
use App\Http\Controllers\Helper\ArrayManager;
use App\Http\Controllers\Helper\StrManager;
use App\Http\Controllers\Share\ShareController;
use App\Http\Controllers\Auth\ProfileController;
use App\Http\Controllers\Helper\FileManager;
use App\Http\Controllers\Search\SearchController;
use App\Http\Controllers\Location\Location;
use App\Http\Controllers\AreaSearch\AreaSearch;
use App\Http\Controllers\Payment\Payment;
// use Torann\GeoIP\GeoIP;
class PropertyController extends Controller
{
    public $request;
    public $propertyFamily;
    public $propertyMess;
    public $propertySublet;
    public $propertyGallery;
    public $extraDefinedController;
    
    public function __construct
    (
        Request $request,
        propertyFamily $propertyFamily,
        propertyMess $propertyMess,
        propertySublet $propertySublet,
        propertyGallery $propertyGallery,
        ExtraDefinedController $extraDefinedController
    )
    {
        $this->request = $request;
        $this->propertyFamily = $propertyFamily;
        $this->propertyMess = $propertyMess;
        $this->propertySublet = $propertySublet;
        $this->propertyGallery = $propertyGallery;
        $this->extraDefinedController = $extraDefinedController;
    }

    public function removeProperty($id = false, $uuid = false)
    {
        $model = (new ExtraDefinedController())->modelArray[$this->request->get('model')];
        $data = $model->where('id', $id)->where('uuid', $uuid)->first();
        $changeModelForGallery = StrManager::changeBackSlash($data->model);
        $gallery = $this->propertyGallery->where('property_id', $id)->where('model', $changeModelForGallery);
        
        $allItem  = $gallery->get();
        foreach($allItem as $file) {
            FileManager::removeFile($file->value);
        }
        $model->where('id', $id)->where('uuid', $uuid)->delete();
        $gallery->delete();

        return response()->json([
            'status' => 'success',
            'code'   => 200,
            'message' => 'Successfully Removed!'
        ]);
    }

    public function removeGalleryItem(Request $request)
    {
        $this->propertyGallery->where('id', $request->id)->delete();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Successfully Removed!'
        ]);
    }

    public function updateUserProperty($types = false)
    {
        if(! $types ) {
            return response()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Property Types Required!'
            ], 404);
        }

        if(! $this->request->get('title')) {
            return reponse()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Property Title Required!'
            ], 404);
        }

        if(! $this->request->get('status')) {
            return reponse()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Property Status Required!'
            ], 404);
        }

        if(! $this->request->get('expiration_date')) {
            return response()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Expiration Date Required!'
            ], 404);
        }

        $dataArray = [
            'title' => $this->request->get('title'),
            'description' => $this->setIfNull('description'),
            'status' => $this->request->get('status'),
            'price' => $this->setIfNull('price'),
            'lat' => $this->setIfNull('lat'),
            'lng' => $this->setIfNull('lng'),
            'address' => $this->setIfNull('address'),
            'area' => $this->setIfNull('area'),
            'city' => $this->setIfNull('city'),
            'zip_code' => $this->setIfNull('zip_code'),
            'size' => $this->setIfNull('size'),
            'rooms' => $this->setIfNull('rooms'),
            'bathrooms' => $this->setIfNull('bathrooms'),
            'bedrooms' => $this->setIfNull('bedrooms'),
            'expiration_date' => $this->setIfNull('expiration_date')
        ];

        $updateProperty = null;

        if ($types == 1) {
            $this->propertyFamily->where('id', $this->request->get('id'))->update($dataArray);
            $updateProperty = propertyFamily::class;
        }
        else if($types == 2) {
            $updateProperty = $this->propertyMess->where('id', $this->request->get('id'))->update($dataArray);
            $updateProperty = propertyMess::class;
        }
        else if($types == 3) {
            $updateProperty = $this->propertySublet->where('id', $this->request->get('id'))->update($dataArray);
            $updateProperty = propertySublet::class;
        }

        if (count($this->request->get('fileList'))) {
            foreach($this->request->get('fileList') as $file) {
                if (! array_key_exists('id', $file)) {
                    $this->propertyGallery->create([
                        'property_id' => $this->request->get('id'),
                        'model' => $updateProperty,
                        'key' => 'image',
                        'value' => $file['response']['fileLink']
                    ]);
                }
            }
        }

        return response()->json([
            'status' => 'success',
            'code'   => 200,
            'message' => 'Successfully Updated!' 
        ]);
    }

    public function userEditProperty($model = false, $uuid = false)
    {
        $property = (object) (new ExtraDefinedController())->propertyType()[$model - 1];
        $property = $property->model->with('user')->where('uuid', $uuid)->first();
        $property->gallery = $this->propertyGallery->where('property_id', $property->id)
                                  ->where('model', StrManager::changeBackSlash($property->model))
                                  ->get();
        $property->property_profile_pic = ArrayManager::setPropertyAvatar($property);
        $property->long_address = StrManager::setAddress($property);
        $property->url = route('property.view', [
            'model' => ArrayManager::getModelIndex($property->model),
            'uuid' => $property->uuid
        ]);
        $property->share = ShareController::page($property->url);
        $property->edit_url = route('property.edit', [
            'model' => ArrayManager::getModelIndex($property->model),
            'uuid' => $property->uuid
        ]);
        $property->is_subscribed = ProfileController::currentUserSubscribed();

        $property->show_phone_number = StrManager::setPhoneNumber(ProfileController::currentUserSubscribed() , ProfileController::getPhoneNumber($property->user_id));

        $property->static_type = (object) (new ExtraDefinedController())->propertyType()[$model - 1];
        
        $property->types = $model;

        return view('home.pages.edit_property', [
            'property' => $property, 
            'property_types' => $model,
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function searchResult()
    {
        $result = SearchController::propertySearch($this->request);
        //return response()->json($result);
        return view('home.pages.search_result', [
            'request' => $this->request->all(),
            'rent' => $result['rent'],
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission() 
        ]);
    }

    public function propertyView($model = false, $uuid = false)
    {
        $property = (object) (new ExtraDefinedController())->propertyType()[$model - 1];
        $property = $property->model->with('user')->where('uuid', $uuid)->first();
        
        $property->gallery = $this->propertyGallery->where('property_id', $property->id)
                                  ->where('model', StrManager::changeBackSlash($property->model))
                                  ->get();
        $property->property_profile_pic = ArrayManager::setPropertyAvatar($property);
        $property->long_address = StrManager::setAddress($property);
        $property->url = route('property.view', [
            'model' => ArrayManager::getModelIndex($property->model),
            'uuid' => $property->uuid
        ]);
        $property->share = ShareController::page($property->url);
        $property->edit_url = route('property.edit', [
            'model' => ArrayManager::getModelIndex($property->model),
            'uuid' => $property->uuid
        ]);
        $property->is_subscribed = ProfileController::currentUserSubscribed();
        
        if ($property->optional_name && $property->optional_phone) {
            $property->optional_status = true;
            $property->optional_avatar = url('static/image/profile.png');
            $property->show_phone_number = StrManager::setPhoneNumber(ProfileController::currentUserSubscribed() , $property->optional_phone);
        } else {
            $property->optional_status = false;
            $property->show_phone_number = StrManager::setPhoneNumber(ProfileController::currentUserSubscribed() , ProfileController::getPhoneNumber($property->user_id));
        }
        

        $property->static_type = (object) (new ExtraDefinedController())->propertyType()[$model - 1];
        
       // return response()->json($property);

        return view('home.pages.single-property', [
            'property' => $property,
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function getPublicUser($uuid)
    {

    }

    public function currentUserProperty()
    {
        $myUserProperty = [];

        foreach($this->extraDefinedController->modelArray as $model) {
            $data = $model->where('user_id', Auth::user()->id)->get();
            foreach($data as $property) {
                $property->gallery = $this->propertyGallery->where('property_id', $property->id)
                                                            ->where('model', StrManager::changeBackSlash($property->model))
                                                            ->get();
                $property->property_profile_pic = ArrayManager::setPropertyAvatar($property);
                $property->long_address = StrManager::setAddress($property);
                $property->url = route('property.view', [
                    'model' => ArrayManager::getModelIndex($property->model),
                    'uuid' => $property->uuid
                ]);
                $property->share = ShareController::page($property->url);
                $property->edit_url = route('property.edit', [
                    'model' => ArrayManager::getModelIndex($property->model),
                    'uuid' => $property->uuid
                ]);
            }
            $myUserProperty[] = $data;
        }

        return response()->json([
            'property' => ArrayManager::removeEmptyIndex($myUserProperty)
        ]);

    }

    public function createProperty()
    {
        //dd($this->request->all());
        if(! $this->request->get('title')) {
            return reponse()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Property Title Required!'
            ], 404);
        }

        if(! $this->request->get('status')) {
            return reponse()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Property Status Required!'
            ], 404);
        }

        if(! $this->request->get('types')) {
            return response()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Property Types Required!'
            ], 404);
        }

        if(! $this->request->get('expiration_date')) {
            return response()->json([
                'status' => 'warning',
                'code' => 404,
                'message' => 'Expiration Date Required!'
            ], 404);
        }

        $dataArray = [
            'uuid' => Uuid::uuid4()->toString(),
            'user_id' =>  Auth::user()->id,
            'title' => $this->request->get('title'),
            'description' => $this->setIfNull('description'),
            'status' => $this->request->get('status'),
            'price' => $this->setIfNull('price'),
            'lat' => $this->setIfNull('lat'),
            'lng' => $this->setIfNull('lng'),
            'address' => $this->setIfNull('address'),
            'area' => $this->setIfNull('area'),
            'city' => $this->setIfNull('city'),
            'zip_code' => $this->setIfNull('zip_code'),
            'size' => $this->setIfNull('size'),
            'rooms' => $this->setIfNull('rooms'),
            'bathrooms' => $this->setIfNull('bathrooms'),
            'bedrooms' => $this->setIfNull('bedrooms'),
            'expiration_date' => $this->setIfNull('expiration_date'),
            'optional_name' => $this->setIfNull('optional_name'),
            'optional_phone' => $this->setIfNull('optional_phone')
        ];

        $createProperty = null;

        if ($this->request->get('types') == 1) {
            $createProperty = $this->propertyFamily->create($dataArray);
            $createProperty->model = propertyFamily::class;
        }
        else if($this->request->get('types') == 2) {
            $createProperty = $this->propertyMess->create($dataArray);
            $createProperty->model = propertyMess::class;
        }
        else if($this->request->get('types') == 3) {
            $createProperty = $this->propertySublet->create($dataArray);
            $createProperty->model = propertySublet::class;
        }

        if (count($this->request->get('fileList'))) {
            foreach($this->request->get('fileList') as $file) {
                $this->propertyGallery->create([
                    'property_id' => $createProperty->id,
                    'model' => $createProperty->model,
                    'key' => 'image',
                    'value' => $file['response']['fileLink']
                ]);
            }
        }


        AreaSearch::mapSearch($this->request, $createProperty);

        return response()->json([
            'status' => 'success',
            'code'   => 200,
            'message' => 'Successfully Created!' 
        ]);
    }

    public function setIfNull($key)
    {
        if ($this->request->get($key)) {
            return $this->request->get($key);
        }

        return null;
    }
}
