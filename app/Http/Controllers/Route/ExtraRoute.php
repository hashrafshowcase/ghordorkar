<?php

namespace App\Http\Controllers\Route;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Payment\Payment;
use App\Http\Controllers\AreaSearch\AreaSearch;
use App\Http\Controllers\Helper\ArrayManager;
use Auth;

class ExtraRoute extends Controller
{
    public function contactPage()
    {
        return view('home.pages.contact_page', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function pricingPage()
    {
        return view('home.pages.pricing', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function privacyPolicy()
    {
        return view('home.pages.privacy_policy', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userFAQ()
    {
        return view('home.pages.faq', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }
}
