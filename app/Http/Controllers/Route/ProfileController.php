<?php

namespace App\Http\Controllers\Route;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Profile\ProfileController as PC;
use App\User;
use App\Http\Controllers\Payment\Payment;
use App\Http\Controllers\AreaSearch\AreaSearch;
use App\Http\Controllers\Helper\ArrayManager;
use Auth;

class ProfileController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userProfile()
    {
        return view('home.pages.profile', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userBookmarks()
    {
        return view('home.pages.bookmarks', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userProperties()
    {
        return view('home.pages.properties', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userNewProperty()
    {
        return view('home.pages.new-property', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userPasswordChanged()
    {
        return view('home.pages.change-password', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userPayNow()
    {
        return view('home.pages.pay_now', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userPaymentHistory()
    {
        return view('home.pages.payment_history', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'history' => Payment::history(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userReferMyFriend()
    {
        return view('home.pages.refer', [
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userSearchArea()
    {
        return view('home.pages.submit-search', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userSearchListing()
    {
        return view('home.pages.search_listing', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    public function userSavedSearchPage()
    {
        return view('home.pages.saved_property', [
            'package' => ArrayManager::userPackage(Auth::user()),
            'isFirst' => Payment::isFirst(),
            'isPaid' => Payment::isPaid(),
            'permission' => AreaSearch::permission()
        ]);
    }

    
}
