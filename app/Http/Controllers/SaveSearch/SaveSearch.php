<?php

namespace App\Http\Controllers\SaveSearch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SavedSearch as SaveSrch;
use App\propertyFamily;
use App\propertyMess;
use App\propertySublet;
use App\propertyGallery;
use App\Http\Controllers\Helper\StrManager;
use App\Http\Controllers\Helper\ArrayManager;
use App\Http\Controllers\Share\ShareController;
use Auth;
use Carbon\Carbon;

class SaveSearch extends Controller
{

    public function getCurrentUserSavedProperty()
    {
        $models = [
            'App\propertyFamily' => (new propertyFamily()),
            'App\propertyMess'   => (new propertyMess()),
            'App\propertySublet' => (new propertySublet())
        ];

        $getSaveProperty = SaveSrch::where('user_id', Auth::user()->id)->where('expired_date', '>=', Carbon::now()->toDateString())->get();

        //return response()->json($getSaveProperty);

        $myUserProperty = [];

        foreach($getSaveProperty as $saved) {
            $model = $models[$saved->model];
            $data = $model->find($saved->property_id);
            $data->gallery = propertyGallery::where('property_id', $saved)
                                                            ->where('model', StrManager::changeBackSlash($data->model))
                                                            ->get();
            $data->property_profile_pic = ArrayManager::setPropertyAvatar($data);
            $data->long_address = StrManager::setAddress($data);
            $data->url = route('property.view', [
                'model' => ArrayManager::getModelIndex($data->model),
                'uuid' => $data->uuid
            ]);
            $data->share = ShareController::page($data->url);
            $data->edit_url = route('property.edit', [
                'model' => ArrayManager::getModelIndex($data->model),
                'uuid' => $data->uuid
            ]);
            $myUserProperty[] = $data;
        }

        return response()->json([
            'property' => $myUserProperty
        ]);
    }
    
    public static function add
    (
        $user,
        $model,
        $property_id,
        $expired_date
    )
    {
        SaveSrch::create([
            'user_id' => $user,
            'model'   => $model,
            'property_id' => $property_id,
            'expired_date' => $expired_date
        ]);
    }

}
