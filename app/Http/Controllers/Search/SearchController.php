<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Controllers\Helper\ExtraDefinedController;
use App\propertyFamily;
use App\propertyMess;
use App\propertySublet;
use App\propertyGallery;
use Carbon\Carbon;
use App\Http\Controllers\Helper\ArrayManager;
use App\Http\Controllers\Helper\StrManager;
use App\Http\Controllers\Share\ShareController;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
class SearchController extends Controller
{

    const EARTH_RADIUS_IN_KM = 6371;
    const EARTH_RADIUS_IN_MILES = 3959;

    public static function propertySearch(Request $request)
    {
        $radius = 1.0; // radius
        $getDataObject = $request->all();
        $model = self::setModel($getDataObject['s_type']);
        $getDataObject = (object) $getDataObject;

        $queryInit = $model->select("*")->with('user');

        if ($getDataObject->max_price && $getDataObject->min_price) {
            $queryInit = $queryInit->whereBetween('price', [$getDataObject->min_price, $getDataObject->max_price]);
        }

        if ($getDataObject->max_area && $getDataObject->min_area) {
            $queryInit = $queryInit->whereBetween('size', [$getDataObject->min_area, $getDataObject->max_area]);
        }

        if ($getDataObject->beds) {
            $queryInit = $queryInit->where('bedrooms', $getDataObject->beds);
        }

        if ($getDataObject->baths) {
            $queryInit = $queryInit->where('bathrooms', $getDataObject->baths);
        }

        $queryInit = $queryInit->where('p_status', 'active')->where('expiration_date', '>=', Carbon::now()->toDateString());

        $rent = $queryInit->whereIn('status', [1, 2])->selectRaw(self::getSql(self::EARTH_RADIUS_IN_KM), [
                                                    $request->get('lat'),
                                                    $request->get('lng'),
                                                    $request->get('lat')
                                                ])
                                                ->havingRaw("distance <= ?", [$radius])
                                                ->orderBy('created_at', 'DESC')
                                                ->simplePaginate(4, ['*'])
                                                ->appends(self::changeNull());
        
        $items = [
            'App\propertyFamily',
            'App\propertyMess',
            'App\propertySublet'
        ];

        foreach($rent as $data) {
            $data->long_status = $data->status == 1 ? 'Searching Home' : 'Requested For Home';
            $data->posted_at = $data->created_at->diffForHumans();
            //$data->expired_at = Carbon::parse($data->expiration_date)->diffForHumans();
            $data->gallery = propertyGallery::where('property_id', $data->id)->where('model', $items[$getDataObject->s_type - 1])->get();
            $data->property_profile_pic = ArrayManager::setPropertyAvatar($data);
            $data->long_address = StrManager::setAddress($data);
            $data->url = route('property.view', [
                'model' => ArrayManager::getModelIndex($data->model),
                'uuid' => $data->uuid
            ]);
            $data->share = ShareController::page($data->url);
            $data->static_type = (object) (new ExtraDefinedController())->propertyType()[$request->get('s_type') - 1];
        }

        return [
            'rent' => $rent,
            'ex' => self::changeNull()
        ];
    }
    
    public static function changeNull()
    {
        $arr = Input::except('page');
        
        foreach($arr as $key => $value) {
            if (is_null( $arr[$key] )) {
                $arr[$key] = '';
            }
        }

        return $arr;
    }

    public static function getSql($r)
    {
        $sql = "( $r * acos( cos( radians(?) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(?) )";
        $sql.= " + sin( radians(?) ) * sin( radians( lat ) ) ) ) AS distance";
        return $sql;
    }

    public static function setModel(int $model)
    {
        $models = [
            'App/propertyFamily',
            'App/propertyMess',
            'App/propertySublet'
        ];

        $el = [
            'App/propertyFamily' => (new propertyFamily()),
            'App/propertyMess' => (new propertyMess()),
            'App/propertySublet' => (new propertySublet())
        ];

        return $el[$models[$model - 1]];
    }
}
