<?php

namespace App\Http\Controllers\Share;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShareController extends Controller
{
    

    public static function page(String $link)
    {
        return [
            'facebook' => [
                'uri' => 'https://www.facebook.com/sharer/sharer.php?u=' . $link,
            ],
            'twitter' => [
                'uri' => 'https://twitter.com/intent/tweet?url=' . $link
            ]
        ];
    }

}
