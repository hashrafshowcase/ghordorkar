<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedSearch extends Model
{

    protected $guarded = ['id'];
}
