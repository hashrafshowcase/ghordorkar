<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class propertyMess extends Model
{
    //use Searchable;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function property_gallery()
    {
        return $this->hasMany(propertyGallery::class);
    }

}
