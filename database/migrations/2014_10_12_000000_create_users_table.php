<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('phone_number')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('avatar')->nullable();
            $table->string('occupation')->nullable();
            $table->text('about_me')->nullable();
            $table->string('payment_id')->nullable();
            $table->enum('notification_mobile', ['active', 'inactive'])->default('active');
            $table->enum('notification_email', ['active', 'inactive'])->default('active');
            $table->enum('user_type', [0, 1, 2])->default(1);
            $table->enum('account_type', [0, 1, 2, 3])->default(0);
            $table->enum('account_status', ['pending', 'verified', 'rejected'])->default('pending');
            $table->enum('is_active', ['pending','active', 'ban'])->default('pending');
            $table->enum('email_verified', ['yes', 'no'])->default('no');
            $table->enum('phone_verified', ['yes', 'no'])->default('no');
            $table->string('mail_verify_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
