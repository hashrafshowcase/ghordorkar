<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyMessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_messes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('model')->default('App/propertyMess');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('status')->nullable()->default(1)->unsigned();
            $table->decimal('price', 10, 2)->nullable();
            $table->decimal('lat', 15, 11)->nullable();
            $table->decimal('lng', 15, 11)->nullable();
            $table->string('address')->nullable();
            $table->string('area')->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->decimal('size', 10, 2)->nullable();
            $table->integer('rooms')->unsigned()->nullable()->default(1);
            $table->integer('bathrooms')->unsigned()->nullable()->default(1);
            $table->integer('bedrooms')->unsigned()->nullable()->default(1);
            $table->enum('p_status', ['active', 'deactive'])->default('active');
            $table->date('expiration_date');
            $table->string('optional_name')->nullable();
            $table->string('optional_phone')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_messes');
    }
}
