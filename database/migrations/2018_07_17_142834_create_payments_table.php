<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('account_type')->unsigned();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('price');
            $table->string('transaction_id');
            $table->enum('type', ['bKash', 'Rocket']);
            $table->enum('status', ['pending', 'active'])->default('active');
            $table->timestamps();

            $table->foreign('user_id')
                 ->references('id')
                 ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
