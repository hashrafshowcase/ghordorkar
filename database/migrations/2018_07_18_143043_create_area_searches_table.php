<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_searches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('types', ['family', 'mess', 'sublet']);
            $table->decimal('max_price', 10, 2)->nullable();
            $table->decimal('min_price', 10, 2)->nullable()->default(2000.00);
            $table->decimal('lat', 15, 11)->nullable();
            $table->decimal('lng', 15, 11)->nullable();
            $table->string('address')->nullable();
            $table->string('area')->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->decimal('max_size', 10, 2)->nullable();
            $table->decimal('min_size', 10, 2)->nullable()->default(1000.00);
            $table->integer('rooms')->unsigned()->nullable()->default(1);
            $table->integer('bathrooms')->unsigned()->nullable()->default(1);
            $table->integer('bedrooms')->unsigned()->nullable()->default(1);
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_searches');
    }
}
