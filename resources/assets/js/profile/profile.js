
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

window.Vue = require('vue');
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
locale.use(lang)

import { Switch, Upload, Notification } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.prototype.$notify = Notification;
Vue.component(Switch.name, Switch);
Vue.component(Upload.name, Upload);
Vue.component('profile', require('./profile.vue'));

const app = new Vue({
    el: '#profile'
});
