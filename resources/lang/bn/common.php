<?php

 return [
    /**
     * Bengeli word 
     */
    "phone" => "৮৮০১৯৭৭৪০৮২৯৭",
    'site_title' => "ঘর দরকার",
    'my_profile' => 'আমার প্রোফাইল',
    'my_bookmark' =>"আমার পুস্তক-চিহ্ন",
    'my_properties' => "আমার প্রোপার্টি",
    'newly_added' => "নতুন যোগ করা হয়েছে",
    'family_house' => "পারিবারিক নিবাস",
    'bachelor_mess' => "ব্যাচেলর মেস",
    'sublet' => "ভাড়া দিয়া থাকেন",
    'what_you_looking_for' => "আপনি কি খুজছেন?",
    "log_out"  => "লগ আউট",
    "bookmarked_listings" => "বুকমার্ক তালিকা",
    "submit_new_property" => "নতুন সম্পত্তি জমা দিন",
    "change_password" => "পাসওয়ার্ড পরিবর্তন করুন",
    "manage_account" => "অ্যাকাউন্ট পরিচালনা করুন",
    "manage_listings" => "তালিকা পরিচালনা করুন",
    "submit_property" => "নতুন তালিকা করুন",
 ];