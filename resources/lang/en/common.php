<?php

 return [
    /**
     * English word 
     */
    "phone" => "8801977408297",
    'site_title' => "GHOR DORKAR",
    'site_motto' => "Find Your Dream Home",
    'what_you_looking_for' => "What are you looking for?",
    'my_profile' => "My Profile",
    'my_bookmark' =>"My Bookmarks",
    'my_properties' => "My Properties",
    'newly_added' => "Newly Added",
    'family_house' => "Family House",
    'bachelor_mess' => "Bachelor Mess",
    'sublet' => "Sublet",
    "log_out"  => "Log Out",
    "bookmarked_listings" => "Bookmarked Listings",
    "submit_new_property" => "Submit New Property",
    "change_password" => "Change Password",
    "manage_account" => "Manage Account",
    "manage_listings" => "Manage Listings",
    "submit_property" => "Submit Property",
 ];