@include('home.partials.header')

	<!-- Titlebar
	================================================== -->
	<div id="titlebar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h2>Log In & Register</h2>
					
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="#">Home</a></li>
							<li>Log In & Register</li>
						</ul>
					</nav>

				</div>
			</div>
		</div>
	</div>


	<!-- Contact
	================================================== -->

	<!-- Container -->
	<div class="container">

		<div class="row">
		<div class="col-md-4 col-md-offset-4">

		<a href="{{ url('/auth/twitter') }}"><button class="button social-login via-twitter"><i class="fa fa-twitter"></i> Log In With Twitter</button></a>
		<!-- <button class="button social-login via-gplus"><i class="fa fa-google-plus"></i> Log In With Google Plus</button> -->
		<a href="{{ url('/auth/facebook') }}"><button class="button social-login via-facebook"><i class="fa fa-facebook"></i> Log In With Facebook</button></a>

		<!--Tab -->
		<!-- <div class="my-account style-1 margin-top-5 margin-bottom-40">

			<ul class="tabs-nav">
				<li class=""><a href="#tab1">Log In</a></li>
				<li><a href="#tab2">Register</a></li>
			</ul>

			<div class="tabs-container alt"> -->

				<!-- Login -->
				<!-- <div class="tab-content" id="tab1" style="display: none;">
					<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="login">
						@csrf
						<p class="form-row form-row-wide">
							<label for="username">Email:
								<i class="im im-icon-Male"></i>
								<input class="input-text" id="username" type="email" name="email" value="{{ old('email') }}" required autofocus/>
							</label>
							@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</p>

						<p class="form-row form-row-wide">
							<label for="password">Password:
								<i class="im im-icon-Lock-2"></i>
								<input class="input-text" type="password" name="password" id="password" required/>
							</label>
							@if ($errors->has('password'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</p>

						<p class="form-row">
							<input type="submit" class="button border margin-top-10" name="login" value="Login" />

							<label for="rememberme" class="rememberme">
							<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label>
						</p>

						<p class="lost_password">
							<a href="#" >Lost Your Password?</a>
						</p>
						
					</form>
				</div> -->

				<!-- Register -->
				<!-- <div class="tab-content" id="tab2" style="display: none;">

					<form method="POST" action="{{ route('register') }}" class="register">
						
						@csrf
								
						<p class="form-row form-row-wide">
							<label for="username2">Name
								<i class="im im-icon-Male"></i>
								<input type="text" class="input-text" id="username2" name="name" value="{{ old('name') }}" required autofocus/>
							</label>
							@if ($errors->has('name'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
						</p>
							
						<p class="form-row form-row-wide">

							<label for="email2">Email Address:
								<i class="im im-icon-Mail"></i>
								<input type="text" class="input-text" id="email2" name="email" value="{{ old('email') }}" required/>
							</label>
								
							@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif

						</p>

						<p class="form-row form-row-wide">
							<label for="password1">Password:
								<i class="im im-icon-Lock-2"></i>
								<input class="input-text" type="password" id="password1" name="password" required/>
							</label>
							@if ($errors->has('password'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</p>

						<p class="form-row form-row-wide">
							<label for="password2">Repeat Password:
								<i class="im im-icon-Lock-2"></i>
								<input class="input-text" type="password" id="password2" name="password_confirmation" required/>
							</label>
						</p>

						<p class="form-row">
							<input type="submit" class="button border fw margin-top-10" name="register" value="Register" />
						</p>

					</form>
				</div> -->

			<!-- </div>
		</div> -->



		</div>
		</div>

	</div>
	<!-- Container / End -->

@include('home.partials.footer')