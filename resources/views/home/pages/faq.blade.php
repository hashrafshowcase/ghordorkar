@include('home.partials.header')


<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>FAQ</h2>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{ url('/') }}">Home</a></li>
						<li>FAQ</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

	<div class="row">

		<div class="col-md-12">
			
			<!-- Toggles Container -->
			<div class="style-2">

				<!-- Toggle 1 -->
				<div class="toggle-wrap">
					<span class="trigger "><a href="#">How do I edit my profile information?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>To edit profile information, first log in to your Ghor Dorkar account. Go to the 'My Profile' page and update the information and then save changes by clicking on the button below.</p>
					</div>
				</div>

				<!-- Toggle 2 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#">Does registration cost the user?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>Registration is free for eveyone but for using certain services user must choose one of three packages available.</p>
					</div>
                </div>
                
                <!-- Toggle 3 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#">How do I add a property in ghordorkar.com?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>To add a property in Ghor Dorkar click on the 'Submit Property' button placed above the page. Fill in the required information and click 'Submit' button to add the property.</p>
					</div>
                </div>
                
                <!-- Toggle 4 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#">How do I delete a property from ghordorkar.com?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>To delete a property, go to the 'My Property' page. This page will show all your added property and delete button is available for each individual property. Confirm delete and the property will be removed.</p>
					</div>
                </div>
                
                <!-- Toggle 5 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#">How do I edit a property in ghordorkar.com?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>To edit a property, go to the 'My Property' page. This page will show all your added property and edit button is available for each individual property. Confirm edit and the property will be editted.</p>
					</div>
                </div>
                
                <!-- Toggle 6 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#">How long will my property ads stay in ghordorkar.com?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>The property ads will stay in ghordorkar.com depending on the user who will post the ads. The user who submits property in ghordorkar.com will limit how long the properties will be available.</p>
					</div>
                </div>
                
                <!-- Toggle 7 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#">Why can’t I see my property in ghordorkar.com?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>Ghor Dorkar reserves the right to remove any property if it is found that the information is not accurate. Contact us for any falsely removed property.</p>
					</div>
                </div>
                
                <!-- Toggle 8 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#">Can i sell property in ghordorkar.com or is it just for renting?<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>Ghor Dorkar allows property to be added only for renting. Any property added in purpose of being sold will be removed automatically without any notice.</p>
					</div>
				</div>

			</div>
			<!-- Toggles Container / End -->
        </div>

	</div>

</div>
<!-- Container / End -->

@include('home.partials.footer')