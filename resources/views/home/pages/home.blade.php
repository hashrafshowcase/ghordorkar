@include('home.partials.header')


<!-- Banner
================================================== -->
<div class="parallax" data-background="images/home-parallax.jpg" data-color="#36383e" data-color-opacity="0.45" data-img-width="2500" data-img-height="1600">
	<div class="parallax-content">

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Main Search Container -->
					<div class="main-search-container">
						<h2>@lang('common.site_motto')</h2>
						
						<!-- Main Search -->
						<form class="main-search-form" action="{{ route('poperty.search') }}" method="GET">
							
							<!-- Type -->
							<div class="search-type">
								<label class="active"><input class="first-tab" checked="checked" name="s_type" type="radio" value="1">Family House</label>
								<label><input name="s_type" type="radio" value="2">Bachelor Mess</label>
								<label><input name="s_type" type="radio" value="3">Sublet</label>
								<div class="search-type-arrow"></div>
							</div>

							<!-- Box -->
							<div class="main-search-box">
									
									<!-- Main Search Input -->
									<div class="main-search-input larger-input">
										<input type="text" name="q" class="ico-01" placeholder="Enter address e.g. street, city and state or zip" value="" id="autocomplete"/>
										<input type="hidden" name="lat" id="lat">
										<input type="hidden" name="lng" id="lng">
										<button class="button">Search</button>
									</div>

									<!-- Row -->
									<div class="row with-forms">

										<!-- Property Type -->
										


										<!-- Min Price -->
										<div class="col-md-6">
											
											<!-- Select Input -->
											<div class="select-input">
												<input name="min_price" type="text" placeholder="Min Price" data-unit="BDT">
											</div>
											<!-- Select Input / End -->

										</div>


										<!-- Max Price -->
										<div class="col-md-6">
											
											<!-- Select Input -->
											<div class="select-input">
												<input name="max_price" type="text" placeholder="Max Price" data-unit="BDT">
											</div>
											<!-- Select Input / End -->

										</div>

									</div>
									<!-- Row / End -->


									<!-- More Search Options -->
									<a href="#" class="more-search-options-trigger" data-open-title="More Options" data-close-title="Less Options"></a>

									<div class="more-search-options">
										<div class="more-search-options-container">

											<!-- Row -->
											<div class="row with-forms">

												<!-- Min Price -->
												<div class="col-md-6">
													
													<!-- Select Input -->
													<div class="select-input">
														<input type="text" name="min_area" placeholder="Min Area" data-unit="Sq Ft">
													</div>
													<!-- Select Input / End -->

												</div>

												<!-- Max Price -->
												<div class="col-md-6">
													
													<!-- Select Input -->
													<div class="select-input">
														<input type="text" name="max_area" placeholder="Max Area" data-unit="Sq Ft">
													</div>
													<!-- Select Input / End -->

												</div>

											</div>
											<!-- Row / End -->


											<!-- Row -->
											<div class="row with-forms">

												<!-- Min Area -->
												<div class="col-md-6">
													<select data-placeholder="Beds" class="chosen-select-no-single" name="beds">
														<option label="blank"></option>	
														<option>Beds (Any)</option>	
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>

												<!-- Max Area -->
												<div class="col-md-6">
													<select data-placeholder="Baths" class="chosen-select-no-single" name="baths">
														<option label="blank"></option>	
														<option>Baths (Any)</option>	
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>

											</div>
											<!-- Row / End -->
	

										</div>
									</div>
									<!-- More Search Options / End -->


								</div>
								<!-- Box / End -->	

						</form>
						<!-- Main Search -->

					</div>
					<!-- Main Search Container / End -->

				</div>
			</div>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">
	
		<!-- <div class="col-md-12">
			<h3 class="headline margin-bottom-25 margin-top-65">Newly Added</h3>
		</div> -->
		
		<!-- Carousel -->
		<!--  -->
		<!-- Carousel / End -->

	</div>
</div>



<!-- Fullwidth Section -->
<section class="fullwidth margin-top-105" data-background-color="#f7f7f7">

	<!-- Box Headline -->
	<h3 class="headline-box">@lang('common.what_you_looking_for')</h3>
	
	<!-- Content -->
	<div class="container">
		<div class="row">

			<div class="col-md-4 col-sm-6">
				<!-- Icon Box -->
				<div class="icon-box-1">

					<div class="icon-container">
						<i class="im im-icon-Office"></i>
						<div class="icon-links">
							<a href="{{ $links['family'] }}">Check</a>
						</div>
					</div>

					<h3>Family House</h3>
					<p>Find Some family house</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<!-- Icon Box -->
				<div class="icon-box-1">

					<div class="icon-container">
						<i class="im im-icon-Home-2"></i>
						<div class="icon-links">
							<a href="{{ $links['mess'] }}">Check</a>
						</div>
					</div>

					<h3>Bachelor Mess</h3>
					<p>Find Some bachelor mess</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<!-- Icon Box -->
				<div class="icon-box-1">

					<div class="icon-container">
						<i class="im im-icon-Office"></i>
						<div class="icon-links">
							<a href="{{ $links['sublet'] }}">Check</a>
						</div>
					</div>

					<h3>Sublet</h3>
					<p>Find Some Sublet House</p>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- Fullwidth Section / End -->






<!-- Flip banner -->
<a href="listings-half-map-grid-standard.html" class="flip-banner parallax" data-background="images/flip-banner-bg.jpg" data-color="#274abb" data-color-opacity="0.9" data-img-width="2500" data-img-height="1600">
	<div class="flip-banner-content">
		<h2 class="flip-visible">We help people and homes find each other</h2>
		<h2 class="flip-hidden">Browse Properties <i class="sl sl-icon-arrow-right"></i></h2>
	</div>
</a>
<!-- Flip banner / End -->


<script>
	var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
    {types: ['geocode']});

	console.log(autocomplete)
    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
	var place = autocomplete.getPlace();
	
	document.getElementById('lat').value = place.geometry.location.lat();
	document.getElementById('lng').value =  place.geometry.location.lng();
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
			}
			document.getElementById('lat').value = geolocation.lat;
			document.getElementById('lng').value =  geolocation.lng;
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
			autocomplete.setBounds(circle.getBounds());
        });
    }
}

geolocate();

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWURC1EHNgmcSScwpIYdegYYcqoUKGDdo&libraries=places&callback=initAutocomplete"
        async defer></script>

@include('home.partials.footer')