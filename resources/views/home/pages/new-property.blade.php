@include('home.partials.header')
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Titlebar
================================================== -->
<div id="titlebar" class="submit-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2><i class="fa fa-plus-circle"></i> Add Property</h2>
			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
<div class="row">

	<!-- Submit Page -->
	<div class="col-md-12" id="new-property">
		<new-property></new-property>
	</div>

</div>
</div>
<script>
window.home = @php 
	echo json_encode([
		'app_url' => env('APP_URL'),
		'user' => auth()->user()->id,
		'token' => csrf_token(),
		'upload_image' => route('upload.image'),
		'upload_file' => route('upload.file'),
		'types' => (new App\Http\Controllers\Helper\ExtraDefinedController())->propertyType(),
		'status' => (new App\Http\Controllers\Helper\ExtraDefinedController())->propertyRentStatus(),
		'create_property' => route('property.create')
	]);
@endphp
</script>

<script src={{mix('js/new-property.js')}} ></script>

@include('home.partials.footer')