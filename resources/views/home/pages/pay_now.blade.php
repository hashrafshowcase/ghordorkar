@include('home.partials.header')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Pay Now</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">@lang('word.home')</a></li>
						<li>@lang('common.bookmarked_listings')</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">


		@include('home.partials.profile-sidebar')

		<div class="col-md-8">
			@if(!$isPaid)
				<div class="notification warning closeable">
					<p>Your Package Expired Please Update your package <a href="{{ route('user.pricing.page') }}" style="text-decoration: underline;">check package list</a></p>
				</div>
			@else
				<div class="notification success">
					<p>Your Payment ID <strong>(GD{{auth()->user()->payment_id}})</strong> .Currently You Use <strong>{{ $package }}</strong> package.</p>
				</div>
			@endif

			<div class="Payment-Bkash">
				<h3>You Can Send Your payment through Bkash (01717408297)</h3>
				<ul>
					<li>Go to your bKash Mobile Menu by dialing *247# Or Your BKash App</li>
					<li>Choose “Send Money”</li>
					<li>Enter the GhorDorkar bKash Account Number (01717408297) you want to pay to </li>
					<li>Enter the amount you want to pay</li>
					<li>Enter a reference against your payment and type your payment id (GD{{auth()->user()->payment_id}}). e.g: GDXXXXXX</li>
					<li>Now enter your bKash Mobile Menu PIN to confirm</li>
					<li>After Payment It will take 1-2 hour for verification.</li>
				</ul>
			</div>

		</div>

	</div>
</div>

@include('home.partials.footer')