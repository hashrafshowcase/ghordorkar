@include('home.partials.header')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Payment History</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">@lang('word.home')</a></li>
						<li>@lang('common.bookmarked_listings')</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">


		@include('home.partials.profile-sidebar')

		<div class="col-md-8">
			<table class="manage-table bookmarks-table responsive-table">

				<tr>
					<th style="width: 0%"><i class="fa fa-file-text"></i> Trx ID</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Type</th>
					<th>Price</th>
				</tr>

				<!-- Item #1 -->
				@foreach($history as $item)
					<tr>
						<td>{{ $item->transaction_id }}</td>
						<td>{{ $item->start_date }}</td>
						<td>{{ $item->end_date }}</td>
						<td>{{ $item->type }}</td>
						<td>{{ $item->price }} BDT</td>
					</tr>
				@endforeach


				

			</table>
		</div>

	</div>
</div>

@include('home.partials.footer')