@include('home.partials.header')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Pricing</h2>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{ url('/') }}">Home</a></li>
						<li>Pricing</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Pricing Tables
================================================== -->

<!-- Container / Start -->
<div class="container">

	<!-- Row / Start -->
	<div class="row">

		<div class="col-md-12">
			<div class="pricing-container margin-top-30">

			<!-- Plan #1 -->

				<div class="plan">

					<div class="plan-price">
						<h3>Basic</h3>
						<span class="value">&#2547 30</span>
						<span class="period">Monthly subsciption fee for 1 listing</span>
					</div>

					<div class="plan-features">
						<ul>
							<li>One Property per Day</li>
							<li>30 Days Availability</li>
							<li>Limited Support</li>
						</ul>
						<a class="button border" href="#">Get Started</a>
					</div>

				</div>

				<!-- Plan #3 -->
				<div class="plan featured">

					<div class="listing-badges">
						<span class="featured">Featured</span>
					</div>

					<div class="plan-price">
						<h3>Extended</h3>
						<span class="value">&#2547 120</span>
						<span class="period">Monthly subsciption fee for 5 listing</span>
					</div>
					<div class="plan-features">
						<ul>
							<li>Five Property per Day</li>
							<li>30 Days Availability</li>
							<li>24/7 Support</li>
						</ul>
						<a class="button" href="#">Get Started</a>
					</div>
				</div>

				<!-- Plan #3 -->
				<div class="plan">

					<div class="plan-price">
						<h3>Professional</h3>
						<span class="value">&#2547 300</span>
						<span class="period">Monthly subsciption fee for 5 listing</span>
					</div>

					<div class="plan-features">
						<ul>
							<li>Ten Property per Day</li>
							<li>30 Days Availability</li>
							<li>24/7 Support</li>
						</ul>
						<a class="button border" href="#">Get Started</a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Row / End -->

</div>
<!-- Container / End -->



@include('home.partials.footer')