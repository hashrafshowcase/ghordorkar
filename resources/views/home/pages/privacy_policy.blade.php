@include('home.partials.header')


<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Privacy Policy</h2>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{ url('/') }}">Home</a></li>
						<li>Privacy Policy</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

	<div class="row">

		<div class="col-md-12">
			
			<!-- Toggles Container -->
			<div class="style-2">

				<!-- Toggle 1 -->
				<div class="toggle-wrap">
					<span class="trigger "><a href="#">First Toggle<i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. Donec ut volutpat metus. Vivamus justo arcu, elementum a sollicitudin pellentesque, tincidunt ac enim. Proin id arcu ut ipsum vestibulum elementum.</p>
					</div>
				</div>

				<!-- Toggle 2 -->
				<div class="toggle-wrap">
					<span class="trigger"><a href="#"> Second Toggle <i class="sl sl-icon-plus"></i></a></span>
					<div class="toggle-container">
						<p>Seded ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. Donec ut volutpat metus. Aliquam tortor lorem, fringilla tempor dignissim at, pretium et arcu.</p>
					</div>
				</div>

			</div>
			<!-- Toggles Container / End -->
        </div>

	</div>

</div>
<!-- Container / End -->

@include('home.partials.footer')