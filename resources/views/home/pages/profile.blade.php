@include('home.partials.header')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>My Profile</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>My Profile</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row">

		@include('home.partials.profile-sidebar')

		<div class="col-md-8" id="profile">
			<profile></profile>
		</div>

	</div>
</div>
<script>
window.home = @php 
	echo json_encode([
		'app_url' => env('APP_URL'),
		'user' => auth()->user()->id,
		'token' => csrf_token(),
		'details' => auth()->user(),
		'update_url' => route('user.update.profile')
	]);
@endphp
</script>

<script src={{mix('js/profile.js')}} ></script>

@include('home.partials.footer')