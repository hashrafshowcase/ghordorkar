@include('home.partials.header')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>My Saved Property</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>My Saved Property</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">

        @include('home.partials.profile-sidebar')

		<div class="col-md-8" id="saved-property">
			<saved-property></saved-property>
		</div>
	</div>
<script>
window.home = @php 
	echo json_encode([
		'app_url' => env('APP_URL'),
		'user' => auth()->user()->id,
		'token' => csrf_token(),
		'my_save_property' => route('get.save.property')
	]);
@endphp
</script>

<script src={{mix('js/saved-property.js')}} ></script>

@include('home.partials.footer')