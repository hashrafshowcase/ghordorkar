@include('home.partials.header')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>My Search Listing</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>My Search Listing</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row">

        @include('home.partials.profile-sidebar')

		<div class="col-md-8" id="search-listing">
			<search-listing></search-listing>
		</div>
	</div>
<script>
window.home = @php 
	echo json_encode([
		'app_url' => env('APP_URL'),
		'user' => auth()->user()->id,
		'token' => csrf_token(),
        'my_search_listing' => route('search.listing.item'), 
        'remove_search_list' => route('search.listing.remove', ['searchId' => '#searchId#'])
	]);
@endphp
</script>

<script src={{mix('js/search-listing.js')}} ></script>

@include('home.partials.footer')