@include('home.partials.header')

<!-- Search
================================================== -->
<section class="search margin-bottom-50">
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<!-- Form -->
			<div class="main-search-box no-shadow">


				<!-- Main Search -->
				<form class="main-search-form" action="{{ route('poperty.search') }}" method="GET">
							
							<!-- Type -->
							<div class="search-type">
								<label class="active"><input class="first-tab" checked="checked" name="s_type" type="radio" value="1">Family House</label>
								<label><input name="s_type" type="radio" value="2">Bachelor Mess</label>
								<label><input name="s_type" type="radio" value="3">Sublet</label>
								<div class="search-type-arrow"></div>
							</div>

							<!-- Box -->
							<div class="main-search-box">
									
									<!-- Main Search Input -->
									<div class="main-search-input larger-input">
										<input type="text" name="q" class="ico-01" placeholder="Enter address e.g. street, city and state or zip" value="" id="autocomplete"/>
										<input type="hidden" name="lat" id="lat">
										<input type="hidden" name="lng" id="lng">
										<button class="button">Search</button>
									</div>

									<!-- Row -->
									<div class="row with-forms">

										<!-- Property Type -->
										


										<!-- Min Price -->
										<div class="col-md-6">
											
											<!-- Select Input -->
											<div class="select-input">
												<input name="min_price" type="text" placeholder="Min Price" data-unit="BDT">
											</div>
											<!-- Select Input / End -->

										</div>


										<!-- Max Price -->
										<div class="col-md-6">
											
											<!-- Select Input -->
											<div class="select-input">
												<input name="max_price" type="text" placeholder="Max Price" data-unit="BDT">
											</div>
											<!-- Select Input / End -->

										</div>

									</div>
									<!-- Row / End -->


									<!-- More Search Options -->
									<a href="#" class="more-search-options-trigger" data-open-title="More Options" data-close-title="Less Options"></a>

									<div class="more-search-options">
										<div class="more-search-options-container">

											<!-- Row -->
											<div class="row with-forms">

												<!-- Min Price -->
												<div class="col-md-6">
													
													<!-- Select Input -->
													<div class="select-input">
														<input type="text" name="min_area" placeholder="Min Area" data-unit="Sq Ft">
													</div>
													<!-- Select Input / End -->

												</div>

												<!-- Max Price -->
												<div class="col-md-6">
													
													<!-- Select Input -->
													<div class="select-input">
														<input type="text" name="max_area" placeholder="Max Area" data-unit="Sq Ft">
													</div>
													<!-- Select Input / End -->

												</div>

											</div>
											<!-- Row / End -->


											<!-- Row -->
											<div class="row with-forms">

												<!-- Min Area -->
												<div class="col-md-6">
													<select data-placeholder="Beds" class="chosen-select-no-single" name="beds">
														<option label="blank"></option>	
														<option>Beds (Any)</option>	
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>

												<!-- Max Area -->
												<div class="col-md-6">
													<select data-placeholder="Baths" class="chosen-select-no-single" name="baths">
														<option label="blank"></option>	
														<option>Baths (Any)</option>	
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>

											</div>
											<!-- Row / End -->
	

										</div>
									</div>
									<!-- More Search Options / End -->


								</div>
								<!-- Box / End -->	

						</form>
						<!-- Main Search -->


			</div>
			<!-- Box / End -->
		</div>
	</div>
</div>
</section>



<!-- Content
================================================== -->
<div class="container">
	<div class="row fullwidth-layout">

		<div class="col-md-12">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-15">
				@if(count($rent))
					<div class="col-md-6">
						<!-- Sort by -->
						<div class="sort-by">
							<h3>Property</h3>
							<!-- <label>Sort by:</label>

							<div class="sort-by-select">
								<select data-placeholder="Default order" class="chosen-select-no-single" >
									<option>Default Order</option>	
									<option>Price Low to High</option>
									<option>Price High to Low</option>
									<option>Newest Properties</option>
									<option>Oldest Properties</option>
								</select>
							</div> -->
						</div>
					</div>
				
					<div class="col-md-6">
						<!-- Layout Switcher -->
						<div class="layout-switcher">
							<a href="#" class="grid-three"><i class="fa fa-th"></i></a>
							<a href="#" class="grid"><i class="fa fa-th-large"></i></a>
							<a href="#" class="list"><i class="fa fa-th-list"></i></a>
						</div>
					</div>
				@endif
			</div>

			
			<!-- Listings -->
			<div class="listings-container grid-layout-three">
				@if(count($rent))
					@foreach($rent as $property)
						<!-- Listing Item -->
						<div class="listing-item">

							<a href="{{ $property->url }}" class="listing-img-container">

								<div class="listing-badges">
									<!-- <span class="featured">Featured</span> -->
									<span>{{ $property->long_status }}</span>
								</div>

								<div class="listing-img-content">
									<span class="listing-price">{{ $property->price ? : 0 }} BDT <i> {{ $property->size ? : 0 }} / sq ft</i></span>
									<!-- <span class="like-icon with-tip" data-tip-content="Add to Bookmarks"></span> -->
									<!-- <span class="compare-button with-tip" data-tip-content="Add to Compare"></span> -->
								</div>

								<div class="listing-carousel">
									<div><img src="{{ $property->property_profile_pic }}" alt=""></div>
								</div>
							</a>
							
							<div class="listing-content">

								<div class="listing-title">
									<h4><a href="{{ $property->url }}">{{ $property->title }}</a></h4>
									<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
										<i class="fa fa-map-marker"></i>
										{{ $property->long_address }}
									</a>

									<a href="{{ $property->url }}" class="details button border">Details</a>
								</div>

								<ul class="listing-details">
									@if($property->size)
										<li>{{ $property->size }} sq ft</li>
									@endif

									@if($property->bedrooms)
										<li>{{ $property->bedrooms }} Bedroom</li>
									@endif

									@if($property->rooms)
										<li>{{ $property->rooms }} Rooms</li>
									@endif
									
									@if($property->bathrooms)
										<li>{{ $property->bathrooms }} Bathroom</li>
									@endif
									
								</ul>

								<div class="listing-footer">
									<span><i class="fa fa-calendar-o"></i> {{ $property->posted_at }}</span>
								</div>

							</div>

						</div>
						<!-- Listing Item / End -->
					@endforeach
				@else
					<div class="notification notice">
						<strong>Sorry!</strong> No Result Found In Your Searching Area.
					</div>
				@endif
			</div>
			<!-- Listings Container / End -->
			
			<div class="clearfix"></div>
			
			<!-- {{ $rent->links() }} -->
			<!-- Pagination -->
			<div class="pagination-container margin-top-20">
				@if($rent->hasPages())
					<nav class="pagination-next-prev">
						<ul>
							@if ($rent->onFirstPage())
								<li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
									<!-- Previous -->
								</li>
							@else
								<li>
									<a href="{{ $rent->previousPageUrl() }}" class="prev" rel="prev" aria-label="@lang('pagination.previous')">Previous</a>
								</li>
							@endif

							@if ($rent->hasMorePages())
								<li>
									<a href="{{ $rent->nextPageUrl() }}" class="next" rel="next" aria-label="@lang('pagination.next')">Next</a>
								</li>
							@else
								<li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
									<!-- <span aria-hidden="true">Next</span> -->
								</li>
							@endif
							<!-- <li><a href="#" class="next">Next</a></li> -->
						</ul>
					</nav>
				@endif
			</div>
			<!-- Pagination / End -->

		</div>

	</div>
</div>

<script>
	var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
    {types: ['geocode']});

	console.log(autocomplete)
    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
	var place = autocomplete.getPlace();
	
	document.getElementById('lat').value = place.geometry.location.lat();
	document.getElementById('lng').value =  place.geometry.location.lng();
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
			}
			document.getElementById('lat').value = geolocation.lat;
			document.getElementById('lng').value =  geolocation.lng;
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
			autocomplete.setBounds(circle.getBounds());
        });
    }
}

geolocate();

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWURC1EHNgmcSScwpIYdegYYcqoUKGDdo&libraries=places&callback=initAutocomplete"
        async defer></script>


@include('home.partials.footer')