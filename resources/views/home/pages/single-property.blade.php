@include('home.partials.header')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="property-titlebar margin-bottom-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<a href="{{ url()->previous() }}" class="back-to-listings"></a>
				<div class="property-title">
					<h2>{{ $property->title }} <span class="property-badge">{{ $property->static_type->label }}</span></h2>
					<span>
						<a href="#location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							{{ $property->long_address }}
						</a>
					</span>
				</div>

				<div class="property-pricing">
					<div class="property-price">{{ $property->price }} BDT</div>
					<div class="sub-price">{{ $property->size }} sq ft</div>
				</div>


			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row margin-bottom-50">
		<div class="col-md-12">
		
			<!-- Slider -->
			<div class="property-slider default">
                @foreach($property->gallery as $avatar)
				    <a href="{{ $avatar->value }}" data-background-image="{{ $avatar->value }}" class="item mfp-gallery"></a>
                @endforeach                
			</div>

			<!-- Slider Thumbs -->
			<div class="property-slider-nav">
                @foreach($property->gallery as $avatar)
                    <div class="item"><img src="{{ $avatar->value }}" alt="{{ $avatar->value }}"></div>
                @endforeach
			</div>

		</div>
	</div>
</div>


<div class="container">
	<div class="row">

		<!-- Property Description -->
		<div class="col-lg-8 col-md-7">
			<div class="property-description">

				<!-- Main Features -->
				<ul class="property-main-features">
					<li>Area <span>{{ $property->size }} sq ft</span></li>
					<li>Rooms <span>{{ $property->rooms }}</span></li>
					<li>Bedrooms <span>{{ $property->bedrooms }}</span></li>
					<li>Bathrooms <span>{{ $property->bathrooms }}</span></li>
				</ul>


				<!-- Description -->
				<h3 class="desc-headline">Description</h3>
				<div class="show-more">
					<p> {{ $property->description }} </p>

					@if($property->description)
						<a href="#" class="show-more-button">Show More <i class="fa fa-angle-down"></i></a>
					@endif
				</div>

                @if($property->is_subscribed)
                    <!-- Location -->
                    <h3 class="desc-headline no-border" id="location">Location</h3>
                    <div id="propertyMap-container">
                        <div id="propertyMap" data-latitude="{{ floatval($property->lat) }}" data-longitude="{{ floatval($property->lng) }}"></div>
                        <a href="#" id="streetView">Street View</a>
                    </div>
                @endif


				<!-- Similar Listings Container -->
				<!-- <h3 class="desc-headline no-border margin-bottom-35 margin-top-60">Similar Properties</h3> -->

				<!-- Layout Switcher -->

				
				<!-- Similar Listings Container / End -->

			</div>
		</div>
		<!-- Property Description / End -->


		<!-- Sidebar -->
		<div class="col-lg-4 col-md-5">
			<div class="sidebar sticky right">

				<!-- Widget -->
				<div class="widget margin-bottom-30">
					<!-- <button class="widget-button with-tip" id="bookmarks" data-tip-content="Add to Bookmarks"><i class="fa fa-star-o"></i></button>
                    <a href="{{ $property->share['facebook']['uri'] }}"><button ><i class="fa fa-facebook"></i></button></a>
                    <a href=""><button class="widget-button with-tip" data-tip-content="Share In Twitter"><i class="fa fa-twitter"></i></button></a> -->
					<div class="clearfix"></div>
				</div>
				<!-- Widget / End -->


				<!-- Widget -->
				<div class="widget">

					<!-- Agent Widget -->
					<div class="agent-widget">
						@if(! $property->optional_status)
							<div class="agent-title">
								<div class="agent-photo"><img src="{{ $property->user->avatar }}" alt="{{ $property->user->name }}" /></div>
								<div class="agent-details">
									<h4><a href="#">{{ $property->user->name }}</a></h4>
									<span><i class="sl sl-icon-call-in"></i>{{ $property->show_phone_number }}</span>
									@guest
									<a href="{{ url('/login') }}"><h5><i class="fa fa-user"></i> Log In / Register </h5></a>
									@endguest
								</div>
								<div class="clearfix"></div>
							</div>
						@else
							<div class="agent-title">
								<div class="agent-photo"><img src="{{ $property->optional_avatar }}" alt="{{ $property->optional_name }}" /></div>
								<div class="agent-details">
									<h4><a href="#">{{ $property->optional_name }}</a></h4>
									<span><i class="sl sl-icon-call-in"></i>{{ $property->show_phone_number }}</span>
									@guest
									<a href="{{ url('/login') }}"><h5><i class="fa fa-user"></i> Log In / Register </h5></a>
									@endguest
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
						<!-- <input type="text" placeholder="Your Email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
						<input type="text" placeholder="Your Phone">
						<textarea>I'm interested in this property [ID 123456] and I'd like to know more details.</textarea>
						<button class="button fullwidth margin-top-5">Send Message</button> -->
					</div>
					<!-- Agent Widget / End -->

				</div>
				<!-- Widget / End -->


				<!-- Widget -->
				
				<!-- Widget / End -->


				<!-- Widget -->
				
				<!-- Widget / End -->

			</div>
		</div>
		<!-- Sidebar / End -->

	</div>
</div>


@include('home.partials.footer')

<!-- Maps -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWURC1EHNgmcSScwpIYdegYYcqoUKGDdo"></script>
<script type="text/javascript" src="{{ asset('js/infobox.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('js/markerclusterer.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/maps.js') }} "></script>
@guest
    <script>
    window.home = @php
        echo json_encode([
            'app_url' => env('APP_URL'),
            'token' => csrf_token(),
            'property' => $property
        ]);
    @endphp
    </script>
@else
    <script>
    window.home = @php
        echo json_encode([
            'app_url' => env('APP_URL'),
            'user' => auth()->user()->id,
            'token' => csrf_token(),
            'property' => $property
        ]);
    @endphp
    </script>
@endguest
<script>
    jQuery('#bookmarks').on('click', function(event) {
        
    })
</script>