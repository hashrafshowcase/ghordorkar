<!-- Footer
================================================== -->
<div class="margin-top-55"></div>

<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo" src="{{ asset('static/image/logo.png') }}" alt="GHOR DORKAR">
				<br><br>
				<p>Ghor Dorkar is a platform to rent and find your next home easily and comfortably. You no longer have to walk around searching for To-Let sign boards. We will do the hard work for you and you can easily find properties to be rented on our website.</p>
			</div>

			<div class="col-md-4 col-sm-6 ">
				<h4>Helpful Links</h4>
				@if(auth()->check())
					<ul class="footer-links">
						<li><a href="{{ route('user.profile') }}">My Account</a></li>
						<li><a href="{{ route('user.properties') }}">My Property</a></li>
					</ul>
				@endif

				<ul class="footer-links">
					<li><a href="{{ route('user.faq') }}">FAQ</a></li>
					<li><a href="{{ route('user.pricing.page') }}">Pricing</a></li>
					<li><a href="{{ route('user.contact.page') }}">Contact</a></li>
					<li><a href="{{ route('user.privacy.policy') }}">Privacy Policy</a></li>
				</ul>
				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
					<span>2/5 Easter Plaza, Amberkhana, Sylhet, Bangladesh</span> <br>
					Phone: <span>+8801977408297</span><br>
					E-Mail:<span> <a href="#"><span class="__cf_email__" >ghordorkar@gmail.com</span></a> </span><br>
				</div>

				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="{{ env('FACEBOOK_PAGE') }}"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="{{ env('TWITTER_PAGE') }}"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="{{ env('GOOGLE_PAGE') }}"><i class="icon-gplus"></i></a></li>
				</ul>

			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">© {{ date("Y") }} GHORDORKAR. All Rights Reserved.
				
				</div>
				
			</div>
		</div>

	</div>

</div>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<script type="text/javascript" src="{{ asset('js/jquery-2.2.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/chosen.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/rangeSlider.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sticky-kit.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/masonry.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/mmenu.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tooltips.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>

</div>
<!-- Wrapper / End -->


</body>

</html>