<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" prefix="og: http://ogp.me/ns#">

    <head>

    <!-- Basic Page Needs
    ================================================== -->
    <title> @lang('common.site_title') </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="p:domain_verify" content="13d4ec411097e3ec75423484039378cf"/>
    <meta name="description" content="Ghor Dorkar is a platform to rent and find your next home easily and comfortably. You no longer have to walk around searching for To-Let sign boards. We will do the hard work for you and you can easily find properties to be rented on our website. We are offering three packages to choose from according to your convenience. Register to www.ghordorkar.com and you can easily find your desired home. You can also post your empty property for rent and we will find you the suitable tenant.">
    <meta name="keywords" content="ghor dorkar, ghordorkar, Ghor Dorkar, ঘর দরকার">
    <meta property="og:title" content="GHOR DORKAR" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('static/image/logo.png') }}">
    <meta name="google-site-verification" content="3WDH-ViK3KR52aBjvB31sVn7t7ysMpgmp8rbRrIrM_Y" />

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/colors/main.css') }}" id="colors">
    
    

    </head>

<body>

<!-- Wrapper -->
<div id="wrapper">


    <!-- Compare Properties Widget
    ================================================== -->
    <!--  -->
    <!-- Compare Properties Widget / End -->


    <!-- Header Container
    ================================================== -->
    <header id="header-container">

        @if(! $isFirst)
            <!-- Topbar -->
            <div id="top-bar" style="background-color: orange;">
                    <div class="container">

                        <div class="middle-side">
                            <p style="margin: 9px 0 6px;position: initial;text-align: center;">Welcome to GhorDorkar, Please Upgrade your account. <a href="{{ url('/pay-now') }}">Click Here</a></p>
                        </div>

                    </div>
                </div>
                <!-- Topbar -->
        @else
            @if(!$isPaid)
                <!-- Topbar -->
                <div id="top-bar" style="background-color: orange;">
                    <div class="container">

                        <div class="middle-side">
                            <p style="margin: 9px 0 6px;position: initial;text-align: center;">
                                Your subscription has expired. Please renew your subscription. <a href="{{ url('/pay-now') }}">Click Here</a></p>
                        </div>

                    </div>
                </div>
                <!-- Topbar -->
            @endif
        @endif

        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Your customer chat code -->
        <div class="fb-customerchat"
        attribution=setup_tool
        page_id="239807286834737"
        theme_color="#0084ff"
        logged_in_greeting="Hi! How can we help you?"
        logged_out_greeting="Hi! How can we help you?">
        </div>

        <div id="top-bar">
            <div class="container">

                <!-- Left Side Content -->
                <div class="left-side">

                    <!-- Top bar -->
                    <ul class="top-bar-menu">
                        <li><i class="fa fa-phone"></i>@lang('common.phone')</li>
                        <li><i class="fa fa-envelope"></i> <a href="#"><span class="__cf_email__" >ghordorkar@gmail.com</span></a></li>
                        <li>
                            <div class="top-bar-dropdown">
                            
                            </div>
                        </li>
                    </ul>

                </div>
                <!-- Left Side Content / End -->


                <!-- Left Side Content -->
                <div class="right-side">

                    <!-- Social Icons -->
                    <ul class="social-icons">
                        <li><a class="facebook" href="{{ env('FACEBOOK_PAGE') }}"><i class="icon-facebook"></i></a></li>
                        <li><a class="twitter" href="{{ env('TWITTER_PAGE') }}"><i class="icon-twitter"></i></a></li>
                        <li><a class="gplus" href="{{ env('GOOGLE_PAGE') }}"><i class="icon-gplus"></i></a></li>
                    </ul>

                </div>
                <!-- Left Side Content / End -->

            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Topbar / End -->


        <!-- Header -->
        <div id="header">
            <div class="container">
                
                <!-- Left Side Content -->
                <div class="left-side">
                    
                    <!-- Logo -->
                    <div id="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('static/image/logo.png') }}" alt="GHOR DORKAR">
                            <!-- <h3 class="m_title" style="font-family: 'Megrim', cursive; margin-top: 10px;">@lang('common.site_title')</h3> -->
                        </a>
                    </div>


                    <!-- Mobile Navigation -->
                    <div class="mmenu-trigger">
                        <button class="hamburger hamburger--collapse" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>


                    <!-- Main Navigation -->
                    <nav id="navigation" class="style-1">
                        <!-- <ul id="responsive">

                            <li><a href="#">Home</a></li>

                            <li>
                                <a href="#">Services</a>
                                <ul>
                                    <li><a href="">BN</a></li>
                                    <li><a href="">EN</a></li>
                                </ul>
                            </li>

                        </ul> -->
                    </nav>
                    <div class="clearfix"></div>
                    <!-- Main Navigation / End -->
                    
                </div>
                <!-- Left Side Content / End -->

                <!-- Right Side Content / End -->
                <div class="right-side">
                    <!-- Header Widget -->
                    <div class="header-widget">
                        
                        @guest
                            <a href="{{ route('login') }}" class="sign-in"><i class="fa fa-user"></i> Log In / Register</a>
                        @else
                            <!-- User Menu -->
                            <div class="user-menu">
                                <div class="user-name"><span><img src="{{ Auth::user()->avatar }}" alt=""></span>{{ Auth::user()->name }}</div>
                                <ul>
                                    <li><a href="#"><i class="sl sl-icon-badge"></i><mark>{{ $package }}</mark></a></li>
                                    <li><a href="{{ route('user.profile') }}"><i class="sl sl-icon-user"></i>@lang('common.my_profile')</a></li>
                                    <!-- <li><a href="{{ route('user.bookmarks') }}"><i class="sl sl-icon-star"></i> Bookmarks</a></li> -->
                                    <li><a href="{{ route('user.properties') }}"><i class="sl sl-icon-docs"></i> My Properties</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="sl sl-icon-power"></i> Log Out
                                        </a>
                                    </li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </div>
                            <a href="{{ route('user.new.properties') }}" class="button border">@lang('common.submit_property')</a>
                        @endguest
                    </div>
                    <!-- Header Widget / End -->
                </div>
                <!-- Right Side Content / End -->

            </div>
        </div>
        <!-- Header / End -->

    </header>
    <div class="clearfix"></div>
    <!-- Header Container / End -->