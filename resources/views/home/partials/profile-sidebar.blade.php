<!-- Widget -->
<div class="col-md-4">
    <div class="sidebar left">

        <div class="my-account-nav-container">
            
            <ul class="my-account-nav">
                <li class="sub-nav-title">@lang('common.manage_account')</li>
                <li><a href="{{ route('user.profile') }}" class="{{ Route::current()->getName() == 'user.profile' ? 'current' : '' }}"><i class="sl sl-icon-user"></i>@lang('common.my_profile')</a></li>
                <!-- <li><a href="{{ route('user.bookmarks') }}" class="{{ Route::current()->getName() == 'user.bookmarks' ? 'current' : '' }}" ><i class="sl sl-icon-star"></i>@lang('common.bookmarked_listings')</a></li> -->
                <!-- <li><a href="{{ route('user.refer.friend') }}" class="{{ Route::current()->getName() == 'user.refer.friend' ? 'current' : '' }}" ><i class="im im-icon-Add-UserStar"></i>Refer My Friend</a></li> -->
            </ul>
            
            <ul class="my-account-nav">
                <li class="sub-nav-title">@lang('common.manage_listings')</li>
                <li><a href="{{ route('user.properties') }}" class="{{ Route::current()->getName() == 'user.properties' ? 'current' : '' }}"><i class="sl sl-icon-docs"></i> @lang('common.my_properties')</a></li>
                <li><a href="{{ route('user.new.properties') }}" class="{{ Route::current()->getName() == 'user.new.properties' ? 'current' : '' }}"><i class="sl sl-icon-action-redo"></i>@lang('common.submit_new_property')</a></li>
                
                @if($isPaid)
                    <li><a href="{{ route('user.search.listing') }}" class="{{ Route::current()->getName() == 'user.search.listing' ? 'current' : '' }}"><i class="sl sl-icon-docs"></i> My Search Listing</a></li>     
                    @if($permission)
                        <li><a href="{{ route('user.new.search_area') }}" class="{{ Route::current()->getName() == 'user.new.search_area' ? 'current' : '' }}"><i class="sl sl-icon-action-redo"></i>Submit New Search Area</a></li>
                    @endif
                @endif                   
            </ul>
            

            @if($isPaid)
                <ul class="my-account-nav">
                    <li class="sub-nav-title">Save Search</li>
                    <li><a href="{{ route('user.saved.property') }}" class="{{ Route::current()->getName() == 'user.saved.property' ? 'current' : '' }}"><i class="sl sl-icon-docs"></i>My Saved Property</a></li>                
                </ul>
            @endif

            <ul class="my-account-nav">
                <li class="sub-nav-title">Payment</li>
                <li><a href="{{ route('user.pay_now') }}" class="{{ Route::current()->getName() == 'user.pay_now' ? 'current' : '' }}"><i class="sl sl-icon-docs"></i> Pay Now</a></li>
                <li><a href="{{ route('user.payment.history') }}" class="{{ Route::current()->getName() == 'user.payment.history' ? 'current' : '' }}"><i class="sl sl-icon-action-redo"></i>Payment History</a></li>
            </ul>

            <ul class="my-account-nav">
                <!-- <li><a href="{{ route('user.change.password') }}" class="{{ Route::current()->getName() == 'user.change.password' ? 'current' : '' }}"><i class="sl sl-icon-lock"></i>@lang('common.change_password')</a></li> -->
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="sl sl-icon-power"></i>@lang('common.log_out')
                    </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
            

        </div>

    </div>
</div>