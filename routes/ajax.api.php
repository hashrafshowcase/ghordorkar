<?php

/*
|---------------------------------------------------------------------------
| Property Routes
|---------------------------------------------------------------------------
*/
Route::post('/property/new-create', 'Property\PropertyController@createProperty')->name('property.create');
// Route::post('/property/{model}/{uuid}');
Route::post('/property/remove-gallery-item', 'Property\PropertyController@removeGalleryItem')->name('property.gallery.remove');
Route::post('/property/update/{types}', 'Property\PropertyController@updateUserProperty')->name('property.update.user');
Route::post('/property/remove/{id}/{uuid}', 'Property\PropertyController@removeProperty')->name('property.remove');
Route::get('/search-listing/all-item', 'AreaSearch\AreaSearch@getCurrentUserSearchArea')->name('search.listing.item');
Route::post('/search-listing/remove/{searchId}', 'AreaSearch\AreaSearch@removeMySearchArea')->name('search.listing.remove');
Route::post('/create-search-listing', 'AreaSearch\AreaSearch@createNewSearchArea')->name('search.listing.create');
Route::get('/get-saved-property', 'SaveSearch\SaveSearch@getCurrentUserSavedProperty')->name('get.save.property');

/*
|---------------------------------------------------------------------------
| Profile Routes
|---------------------------------------------------------------------------
*/
Route::post('/update-my-profile', 'Profile\ProfileController@updateProfile')->name('user.update.profile');
Route::get('/get-all-properties', 'Property\PropertyController@currentUserProperty')->name('my.user.properties');