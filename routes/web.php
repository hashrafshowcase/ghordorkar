<?php

use Spatie\Sitemap\SitemapGenerator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/
Auth::routes();
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('/', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| Profile
|--------------------------------------------------------------------------
*/
Route::get('/user/{uuid?}', 'Profile\ProfileController@getPublicUser')->name('public.user.profile');

Route::get('/my-profile', 'Route\ProfileController@userProfile')->name('user.profile');

Route::get('/my-bookmarks', 'Route\ProfileController@userBookmarks')->name('user.bookmarks');

Route::get('/my-properties', 'Route\ProfileController@userProperties')->name('user.properties');

Route::get('/new-property', 'Route\ProfileController@userNewProperty')->name('user.new.properties');

Route::get('/change-password', 'Route\ProfileController@userPasswordChanged')->name('user.change.password');

Route::get('/property/view/{model?}/{uuid?}', 'Property\PropertyController@propertyView')->name('property.view');

Route::get('/property/edit/{model?}/{uuid?}', 'Property\PropertyController@userEditProperty')->name('property.edit');

Route::get('/pay-now', 'Route\ProfileController@userPayNow')->name('user.pay_now');

Route::get('/payment-history', 'Route\ProfileController@userPaymentHistory')->name('user.payment.history');

Route::get('/refer-my-friend', 'Route\ProfileController@userReferMyFriend')->name('user.refer.friend');

Route::get('/search-result', 'Property\PropertyController@searchResult')->name('poperty.search');

Route::get('/privacy-policy', 'Route\ExtraRoute@privacyPolicy')->name('user.privacy.policy');

Route::get('/submit-new-search', 'Route\ProfileController@userSearchArea')->name('user.new.search_area');

Route::get('/my-search-listing', 'Route\ProfileController@userSearchListing')->name('user.search.listing');

Route::get('/my-saved-property', 'Route\ProfileController@userSavedSearchPage')->name('user.saved.property');

Route::get('/contact-us', 'Route\ExtraRoute@contactPage')->name('user.contact.page');

Route::get('/pricing', 'Route\ExtraRoute@pricingPage')->name('user.pricing.page');

Route::get('/faq', 'Route\ExtraRoute@userFAQ')->name('user.faq');

Route::get('/site-map', function() {
    SitemapGenerator::create('https://ghordorkar.com')->writeToFile(public_path('sitemap.xml'));
});

/*
|--------------------------------------------------------------------------
| File Upload
|--------------------------------------------------------------------------
*/
Route::post('/upload-image', 'Helper\FileUploadController@uploadImage')->name('upload.image');
Route::post('/upload-file', 'Helper\FileUploadController@uploadFile')->name('upload.file');

require 'ajax.api.php';