let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix //.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/property/property.js', 'public/js' )
   .js('resources/assets/js/new-property/new-property.js', 'public/js')
   .js('resources/assets/js/edit-property/edit-property.js', 'public/js')
   .js('resources/assets/js/profile/profile.js', 'public/js')
   .js('public/dev-js/search-map.js', 'public/js')
   .js('resources/assets/js/search-listing/search-listing.js', 'public/js')
   .js('resources/assets/js/new-search-listing/new-search-listing.js', 'public/js')
   .js('resources/assets/js/saved-property/saved-property.js', 'public/js')
   //.sass('resources/assets/sass/app.scss', 'public/css');
